<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'taskName' => $this->taskName,
            'description' => $this->description,
            'status' => $this->status,
            'date_create' => $this->created_at->format('d/m/Y'),
            'date_update' => $this->updated_at->format('d/m/Y'),
        ];
    }
}
