<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tasks\StoreTaskRequest;
use App\Http\Requests\Tasks\UpdateTaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Services\Tasks\TaskServices;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected TaskServices $taskServices;

    public function __construct(TaskServices $taskServices)
    {
        $this->taskServices = $taskServices;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json(TaskResource::collection($this->taskServices->getAllTasks()));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTaskRequest $request)
    {
        return response()->json(new TaskResource($this->taskServices->storeTask($request->validated())));
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        return response()->json(new TaskResource($this->taskServices->getTask($task)));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        return response()->json(new TaskResource($this->taskServices->updateTask($request->validated(), $task)));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        $this->taskServices->deleteTask($task);
        return response()->json(null, 204);
    }

    public function filterByStatus($status)
    {
        return response()->json(TaskResource::collection($this->taskServices->filterByStatus($status)));
    }

    public function filterByDate($date)
    {
        return response()->json(TaskResource::collection($this->taskServices->filterByDate($date)));
    }
}
