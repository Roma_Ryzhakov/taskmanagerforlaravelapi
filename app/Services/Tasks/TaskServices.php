<?php

namespace App\Services\Tasks;

use App\Models\Task;

class TaskServices
{
    public function getAllTasks()
    {
        return Task::all();
    }

    public function getTask($task)
    {
        return Task::findOrFail($task->id);
    }

    public function storeTask($request)
    {
        return Task::create($request);
    }

    public function updateTask($request, $task)
    {
        $task = Task::findOrFail($task->id);
        $task -> update($request);
        return $task;
    }

    public function deleteTask($task)
    {
        $task = Task::findOrFail($task->id);
        $task->delete();
        return null;
    }

    public function filterByStatus($status)
    {
        $task = Task::query()->where('status', $status)->get();
        return $task;
    }

    public function filterByDate($date)
    {
        $task = Task::query()->whereDate('created_at', $date)->get();
        return $task;
    }
}
